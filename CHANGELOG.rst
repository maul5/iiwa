^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package iiwa
^^^^^^^^^^^^^^^^^^^^^^^^^^

2.0.1 (2015-04-09)
------------------
* renamed package to make consistent with ros standards
* Merged in virgaS/iiwa/sunrise1.5 (pull request #1)
  Fix for Sunrise.OS 1.5+ and comments
* Code to fix for Sunrise.OS 1.5+ and comments
* The basic control interface for the IIWA robot
